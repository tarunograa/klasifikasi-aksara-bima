package backend;

import java.util.ArrayList;

import kasirkeren.Beranda;
import kasirkeren.Item;
import model.Order;

public class BerandaCtrl {
    public BerandaCtrl(){

    }


    public void loadOrder(ArrayList<Order> orders){
        ArrayList<Item> orderItems = new ArrayList<Item>();
        for(Order order: orders){
            orderItems.add(new Item(order));
        }

        for (Item item: orderItems){
            Beranda.jPanel1.add(item);
            Beranda.jPanel1.revalidate();
            Beranda.jPanel1.repaint();
        }
    }



}
