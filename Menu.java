

package backend;

import javax.swing.BoxLayout;

import model.Order;
import model.Product;
import java.util.ArrayList;

import kasirkeren.PilihProduk;
import kasirkeren.PilihanItem;


public class Menu {
    ArrayList<PilihanItem> product;
    public void showMenu(PilihProduk pilihProduk){
        this.product = showProducts();
        for(PilihanItem item: this.product){
            pilihProduk.getJPanel1().add(item);
        }
    }

    public ArrayList<Product> createProducts(){
        ArrayList<Product> products = new ArrayList<Product>();
        products.add(new Product("Mie Sedaap Goreng", 3500));
        products.add(new Product("Mie Sedaap Soto", 3000));
        products.add(new Product("Mizone", 5000));
        products.add(new Product("Teh Rio", 1000));
        products.add(new Product("Kopikap", 1500));
        products.add(new Product("Yakult", 2000));
        products.add(new Product("Nutribost", 6000));
        products.add(new Product("Kuda Mas Original", 2000));
        products.add(new Product("Nabati Coklat", 2000));
        products.add(new Product("Nabati Permen Karet", 2000));

        return products;
    }

    public ArrayList<PilihanItem> showProducts(){
        ArrayList<Product> products= createProducts();
        ArrayList<PilihanItem> productsView = new ArrayList<PilihanItem>();
        
        for (Product product: products){
            PilihanItem item = new PilihanItem(product);
            productsView.add(item);
        }

        return productsView;
        
    }
    
    public ArrayList<Order> pilihClicked(){
        ArrayList<Order> order = new ArrayList<Order>();
        for(PilihanItem item: this.product){
            if(item.getJCheckBox1().isSelected()){
                order.add(new Order(item.getProduct()));
                // System.out.println(item.getProduct().getName());
            }
        }

        return order;
    }
}
